package cli

import (
	"io"
	"log"
	"log/slog"
	"os"

	"github.com/oklog/ulid/v2"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"go.uber.org/zap"
	"go.uber.org/zap/exp/zapslog"
	"go.uber.org/zap/zapcore"
)

// OptConfigFile names a runtime option.
const OptConfigFile = "config"

// OptVerboseOutput names a runtime option.
const OptVerboseOutput = "verbose"

// OptDebugMode names a runtime option.
const OptDebugMode = "debug"

// OptQuietMode names a runtime option.
const OptQuietMode = "quiet"

// OptLogOutput names a runtime option.
const OptLogOutput = "log"

// OptLogJSONConsole logs fully-JSON-encoded output to the console
const OptLogJSONConsole = "json_console"

// CLIEnvironment contains common CLI runtime configuration values.
type CLIEnvironment struct {
	ConfigSource string
	Verbose      bool
	Debug        bool
	Quiet        bool
	Closers      map[string]io.Closer
	RunID        string
	otherCloseFn []func()
	logWriter    io.Writer
}

// Close attempts to close the contents of Closers.
func (e *CLIEnvironment) Close() {
	for n, c := range e.Closers {
		cerr := c.Close()
		if cerr != nil {
			log.Printf("Error closing '%s': %s\n", n, cerr.Error())
		}
	}
	for _, cfn := range e.otherCloseFn {
		cfn()
	}
}

// BindViperCommand walks a Cobra command tree, binding flags to Viper.
func BindViperCommand(cmd *cobra.Command) {
	viper.AutomaticEnv() // read in environment variables that match
	viper.BindPFlags(cmd.LocalFlags())
	if cmd.HasSubCommands() {
		for _, sc := range cmd.Commands() {
			BindViperCommand(sc)
		}
	}
}

// PrepViperCommandForLogging adds quiet/debug/verbose flags.
func PrepViperCommandForLogging(cmd *cobra.Command) {
	cmd.PersistentFlags().Bool(OptVerboseOutput, false, "be more verbose in logging")
	cmd.PersistentFlags().Bool(OptDebugMode, false, "log debug information")
	cmd.PersistentFlags().Bool(OptQuietMode, false, "be less verbose in logging")
	cmd.PersistentFlags().String(OptLogOutput, "", "log file (default is STDERR)")
	cmd.PersistentFlags().Bool(OptLogJSONConsole, false, "send fully-JSON-encoded log messages to console")
}

func coreReadViperEnvironment(explicitConfigFile string, configName string, configPath ...string) (env CLIEnvironment) {

	if explicitConfigFile == "" {
		explicitConfigFile = viper.GetString(OptConfigFile)
	}

	if explicitConfigFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(explicitConfigFile)
	} else {
		viper.SetConfigName(configName)
		for _, p := range configPath {
			viper.AddConfigPath(p)
		}
	}

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		explicitConfigFile = viper.ConfigFileUsed()
	}

	env.Debug = viper.GetBool(OptDebugMode)
	env.Verbose = viper.GetBool(OptVerboseOutput)
	env.Quiet = viper.GetBool(OptQuietMode)
	env.Closers = make(map[string]io.Closer)
	env.RunID = ulid.Make().String()

	if explicitConfigFile != "" {
		env.ConfigSource = explicitConfigFile
	}

	return
}

// StdReadViperEnvironment reads Viper configuration and returns a CLIEnvironment struct
func StdReadViperEnvironment(explicitConfigFile string, configName string, configPath ...string) CLIEnvironment {
	return coreReadViperEnvironment(explicitConfigFile, configName, configPath...)
}

func ReadEnvConfigureZap(explicitConfigFile string, configName string, sugar map[string]string, configPath ...string) (env CLIEnvironment) {
	env = StdReadViperEnvironment(explicitConfigFile, configName, configPath...)

	var encoderConfig zapcore.EncoderConfig
	if env.Debug {
		encoderConfig = zap.NewDevelopmentEncoderConfig()
	} else {
		encoderConfig = zap.NewProductionEncoderConfig()
	}
	encoderConfig.EncodeTime = zapcore.RFC3339NanoTimeEncoder

	var filter zap.LevelEnablerFunc
	if env.Debug {
		filter = zap.LevelEnablerFunc(func(level zapcore.Level) bool {
			return true
		})
	} else {
		filter = zap.LevelEnablerFunc(func(level zapcore.Level) bool {
			return level >= zapcore.InfoLevel
		})
	}

	var zenc zapcore.Encoder
	if viper.GetBool(OptLogJSONConsole) {
		zenc = zapcore.NewJSONEncoder(encoderConfig)
	} else {
		zenc = zapcore.NewConsoleEncoder(encoderConfig)
	}

	core := zapcore.NewCore(zenc, zapcore.AddSync(os.Stderr), filter)

	logFile := viper.GetString(OptLogOutput)
	if logFile != "" {
		logWriter, lferr := os.Create(logFile)
		if lferr != nil {
			log.Fatal("error opening log file", logFile, lferr)
		}

		env.Closers[OptLogOutput] = logWriter

		fcore := zapcore.NewCore(zapcore.NewJSONEncoder(encoderConfig), zapcore.AddSync(logWriter), filter)
		if !env.Quiet {
			core = zapcore.NewTee(core, fcore)
		} else {
			core = fcore
		}
	}

	var rootLogger *zap.Logger
	if env.Debug {
		rootLogger = zap.New(core, zap.AddCaller(), zap.AddStacktrace(zapcore.WarnLevel))
	} else {
		rootLogger = zap.New(core, zap.AddCaller(), zap.AddStacktrace(zapcore.ErrorLevel))
	}
	rootLogger = rootLogger.With(
		zap.String("svc", configName),
		zap.String("run", env.RunID))
	sl := slog.New(zapslog.NewHandler(core, nil)).With(
		slog.String("svc", configName),
		slog.String("run", env.RunID))

	for k, v := range sugar {
		rootLogger = rootLogger.With(zap.String(k, v))
		sl = sl.With(slog.String(k, v))
	}

	restoreGlobals := zap.ReplaceGlobals(rootLogger)
	env.otherCloseFn = append(env.otherCloseFn,
		func() {
			rootLogger.Sync()
			restoreGlobals()
		})

	slog.SetDefault(sl)

	return
}
