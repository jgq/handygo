package handygo

import (
	"errors"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/gofrs/uuid"
)

// ZeroTime returns a zero value time.Time struct.
func ZeroTime() (zt time.Time) {
	return
}

// MSTime converts a .Net-formatted JSON Date string into a time.Time struct.
func MSTime(msdate string) (gt time.Time, err error) {

	if msdate == "" {
		err = errors.New("empty string provided")
		return
	}

	opar := strings.Index(msdate, "(")
	tzpm := strings.IndexAny(msdate, "+-")
	cpar := strings.Index(msdate, ")")

	if cpar-tzpm != 5 {
		err = errors.New("unexpected format provided")
		return
	}

	var millis int64
	millis, err = strconv.ParseInt(msdate[opar+1:tzpm], 10, 64)
	if err != nil {
		return
	}

	gt = time.Unix(0, millis*int64(time.Millisecond))

	var tzh, tzm int
	var tzn int64
	tzh, err = strconv.Atoi(msdate[tzpm : tzpm+3])
	if err != nil {
		return
	}
	tzm, err = strconv.Atoi(msdate[cpar-2 : cpar])
	if err != nil {
		return
	}

	tzn = int64(tzh) * int64(time.Hour)
	tzn = tzn + (int64(tzm) * int64(time.Minute))

	gt = gt.In(time.FixedZone("", int(int64(tzn)/int64(time.Second))))

	return
}

func sortedPropertyKeys(m map[string]string) (l []string) {
	for k := range m {
		l = append(l, k)
	}
	sort.Strings(l)
	return
}

// PropertyNamesDiffer compares the number and names of provided map keys, returning true when a difference is detected.
func PropertyNamesDiffer(a map[string]string, b map[string]string) bool {

	if a == nil && b == nil {
		return false
	}

	if a == nil || b == nil {
		return true
	}

	if len(a) != len(b) {
		// Different number of keys indicates different maps.
		return true
	}

	akl := sortedPropertyKeys(a)
	bkl := sortedPropertyKeys(b)

	if strings.Join(akl, ",") != strings.Join(bkl, ",") {
		// Difference in key names detected.
		return true
	}

	return false
}

// PropertiesDiffer compares the keyed values of provided maps (only if PropertyNamesDiffer returns false), returning true when a difference is detected.
func PropertiesDiffer(a map[string]string, b map[string]string) bool {

	if PropertyNamesDiffer(a, b) {
		return true
	}

	for k, v := range a {
		if v != b[k] {
			return true
		}
	}

	return false
}

func UUIDStringOrEmpty(id uuid.UUID) string {
	if id == uuid.Nil {
		return ""
	} else {
		return id.String()
	}
}

func PtrUUIDStringOrEmpty(id *uuid.UUID) string {
	if id == nil {
		return ""
	} else if *id == uuid.Nil {
		return ""
	} else {
		return id.String()
	}
}

func UUIDFromStringOrNil(id string) *uuid.UUID {
	if id == "" {
		return nil
	}

	uid, err := uuid.FromString(id)
	if err != nil {
		return nil
	}

	return &uid
}

func StringsFromUUIDs(source []uuid.UUID) (result []string) {
	for _, su := range source {
		if su == uuid.Nil {
			continue
		}
		result = append(result, su.String())
	}
	return
}

func UUIDsFromStrings(source []string) (result []uuid.UUID, err error) {
	for _, si := range source {
		var su uuid.UUID
		su, err = uuid.FromString(si)
		if err != nil {
			return
		}
		result = append(result, su)
	}
	return
}
