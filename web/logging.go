package web

import (
	"context"
	"log/slog"
	"net/http"
	"time"

	"github.com/oklog/ulid/v2"

	"jgquinn.com/handygo"
)

type RequestIDAttrType string
type RequestStartAttrType string

const RequestIDAttr RequestIDAttrType = "req.id"
const RequestStartAttr RequestStartAttrType = "req.start"

const (
	requestLogGroup    = "req"
	requestIDField     = "id"
	requestStartField  = "start"
	requestMethodField = "method"
	requestURIField    = "uri"
	requestHostField   = "host"

	responseLogGroup        = "res"
	responseElapsedField    = "elapsed"
	responseStatusCodeField = "status"
)

func ContextWithRequestStartTime(base context.Context) context.Context {
	start := base.Value(RequestStartAttr)
	if start != nil {
		return base
	}

	now := time.Now()
	start = &now
	return context.WithValue(base, RequestStartAttr, start)
}

func StartTimeFromContext(ctx context.Context) *time.Time {
	if start := ctx.Value(RequestStartAttr); start != nil {
		if stime, ok := start.(*time.Time); ok {
			return stime
		}
	}
	return nil
}

func ContextWithRequestID(base context.Context) context.Context {
	if reqID := base.Value(RequestIDAttr); reqID != nil {
		return base
	}

	id := ulid.Make().String()
	return context.WithValue(base, RequestIDAttr, id)
}

func RequestIDFromContext(ctx context.Context) string {
	if reqID := ctx.Value(RequestIDAttr); reqID != nil {
		if rids, ok := reqID.(string); ok {
			return rids
		}
	}
	return ""
}

func requestWithStart(r *http.Request, updateLogger bool) *http.Request {
	ctx := ContextWithRequestStartTime(r.Context())
	start := StartTimeFromContext(ctx)

	if updateLogger {
		if rqlg := handygo.LoggerFromContext(ctx); rqlg != nil {
			rqlg = rqlg.With(slog.Group(requestLogGroup, slog.Time(requestStartField, *start)))
			ctx = handygo.ContextWithLogger(ctx, rqlg)
		}
	}

	return r.WithContext(ctx)
}

func requestWithID(r *http.Request, updateLogger bool) *http.Request {
	ctx := ContextWithRequestID(r.Context())
	reqID := RequestIDFromContext(ctx)

	if updateLogger {
		if rqlg := handygo.LoggerFromContext(ctx); rqlg != nil {
			rqlg = rqlg.With(slog.Group(requestLogGroup, slog.String(requestIDField, reqID)))
			ctx = handygo.ContextWithLogger(ctx, rqlg)
		}
	}

	return r.WithContext(ctx)
}

func WithRequestStart(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		next.ServeHTTP(w, requestWithStart(r, true))
	})
}

func WithRequestID(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		next.ServeHTTP(w, requestWithID(r, true))
	})
}

func WithSLogging(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		r = requestWithStart(r, false)
		r = requestWithID(r, false)

		ctx := r.Context()
		id := RequestIDFromContext(ctx)
		start := StartTimeFromContext(ctx)

		rqlg := handygo.LoggerFromContext(ctx)
		if rqlg == nil {
			rqlg = slog.Default()
		}
		rqlg = rqlg.With(
			slog.Group(requestLogGroup,
				slog.String(requestIDField, id),
				slog.Time(requestStartField, *start),
				slog.String(requestMethodField, r.Method),
				slog.String(requestURIField, r.RequestURI),
				slog.String(requestHostField, r.Host),
			))
		rqlg.Debug("processing request")

		scw := NewStatusCodeWriter(w)

		defer func() {
			if rcv := recover(); rcv != nil {
				rqlg.Error("panic while processing request",
					slog.Any("recovered", rcv),
					slog.Group(responseLogGroup,
						slog.Duration(responseElapsedField, time.Since(*start)),
					))
				scw.WriteHeader(http.StatusInternalServerError)
			}
		}()

		next.ServeHTTP(scw, r.WithContext(handygo.ContextWithLogger(ctx, rqlg)))

		rqlg.Info("processed request",
			slog.Group(responseLogGroup,
				slog.Int(responseStatusCodeField, scw.StatusCode()),
				slog.Duration(responseElapsedField, time.Since(*start)),
			))
	})
}
