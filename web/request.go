package web

import "net/http"

type statusRecorder struct {
	http.ResponseWriter
	statusCode int
}

func (rec *statusRecorder) WriteHeader(code int) {
	rec.statusCode = code
	rec.ResponseWriter.WriteHeader(code)
}

func (rec *statusRecorder) StatusCode() int {
	return rec.statusCode
}

type StatusCodeWriter interface {
	http.ResponseWriter
	StatusCode() int
}

func NewStatusCodeWriter(w http.ResponseWriter) StatusCodeWriter {
	return &statusRecorder{
		ResponseWriter: w,
		statusCode:     http.StatusOK,
	}
}
