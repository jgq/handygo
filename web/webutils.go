package web

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"
)

// WriteJSONResponse sets the response content-type and writes payload as JSON.
func WriteJSONResponse(w http.ResponseWriter, payload interface{}) {
	w.Header().Set("Content-type", "application/json")
	ErrorFailRequest(json.NewEncoder(w).Encode(payload), w)
}

// ReportErrorCode sends an error HTTP response.
func ReportErrorCode(sc int, msg string, w http.ResponseWriter) {
	http.Error(w, msg, sc)
}

// ReportError sends msg as an error HTTP response.
func ReportError(msg string, w http.ResponseWriter) {
	ReportErrorCode(http.StatusInternalServerError, msg, w)
}

// ErrorFailRequest sends an error HTTP response if err is not nil.
func ErrorFailRequest(err error, w http.ResponseWriter) bool {
	if err != nil {
		log.Print(err)
		ReportError(err.Error(), w)
	}
	return err != nil
}

// PanicFailRequest sends an error HTTP response if a panic is recovered.
func PanicFailRequest(w http.ResponseWriter) {
	if p := recover(); p != nil {
		e, ok := p.(error)
		if !ok {
			e = fmt.Errorf("pkg: %v", p)
		}
		ErrorFailRequest(e, w)
	}
}

func AbsoluteURL(r *http.Request) string {
	if r.URL.IsAbs() {
		return r.URL.String()
	}
	sb := strings.Builder{}
	sb.WriteString(r.Proto)
	sb.WriteString("://")
	sb.WriteString(r.Host)
	sb.WriteString("/")
	sb.WriteString(r.URL.Path)
	return sb.String()
}
