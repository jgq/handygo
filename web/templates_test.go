package web

import (
	"testing"
	"time"
)

func TestLimitString(t *testing.T) {
	longstring := "123456789"
	targetstring := "123"
	result := LimitString(longstring, 3)
	if result != targetstring {
		t.Errorf("unexpected result string: %s", result)
	}

	shortstring := "12"
	targetstring = shortstring
	result = LimitString(shortstring, 3)
	if result != targetstring {
		t.Errorf("unexpected result string: %s", result)
	}
}

func TestFormatDate(t *testing.T) {
	dt := time.Time{}
	targetstring := ""
	result := FormatDate(dt, "2006-01-02")
	if result != targetstring {
		t.Errorf("unexpected result string: %s", result)
	}

	dt = time.Now()
	result = FormatDate(dt, "2006-01-02")
	if len(result) < 10 {
		t.Errorf("unexpected result string: %s", result)
	}
}
