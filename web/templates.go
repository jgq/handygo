package web

import (
	"html/template"
	"time"

	"jgquinn.com/handygo"
)

// LimitString truncates a source string to the specified character limit if necessary.
func LimitString(source string, limit int) string {
	runes := []rune(source)
	if len(runes) > limit {
		return string(runes[:limit])
	}
	return source
}

// FormatDate formats a time.Time unless it is a zero value.
func FormatDate(date time.Time, format string) (formatted string) {
	if date != handygo.ZeroTime() {
		formatted = date.Format(format)
	}
	return
}

var templateFuncMap template.FuncMap

func init() {
	templateFuncMap = template.FuncMap{
		"Limit":      LimitString,
		"FormatDate": FormatDate,
	}
}

// TemplateFunctions returns a template FuncMap with handy text processing functions.
func TemplateFunctions() template.FuncMap {
	return templateFuncMap
}
