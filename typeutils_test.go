package handygo

import (
	"testing"
	"time"
)

func testPropertyMaps() (a map[string]string, a2 map[string]string, a3 map[string]string, b map[string]string, c map[string]string) {

	a = map[string]string{
		"one":   "un",
		"two":   "deux",
		"three": "trois",
	}

	a2 = map[string]string{
		"one":   "un",
		"two":   "deux",
		"three": "trois",
	}

	a3 = map[string]string{
		"one":   "un",
		"two":   "deux",
		"three": "trois",
		"four":  "quatre",
	}

	b = map[string]string{
		"one":   "uno",
		"two":   "dos",
		"three": "tres",
	}

	c = map[string]string{
		"one":  "un",
		"two":  "deux",
		"zero": "zero",
	}

	return
}

func TestPropertyNamesDiffer(t *testing.T) {
	a, _, a3, b, c := testPropertyMaps()
	if PropertyNamesDiffer(a, b) {
		t.Error("maps with matching property names incorrectly identified as different")
	}
	if !PropertyNamesDiffer(a, a3) {
		t.Error("maps with different numbers of property names incorrectly identified as same")
	}
	if !PropertyNamesDiffer(a, c) {
		t.Error("maps with different property names incorrectly identified as same")
	}
	if PropertyNamesDiffer(nil, nil) {
		t.Error("nil maps incorrectly identified as different")
	}
	if !PropertyNamesDiffer(a, nil) {
		t.Error("populated map inccorectly identified as matching nil map")
	}
}

func TestPropertiesDiffer(t *testing.T) {
	a, a2, _, b, _ := testPropertyMaps()
	if PropertiesDiffer(a, a2) {
		t.Error("matching maps incorrectly idenfitied as different")
	}
	if !PropertiesDiffer(a, b) {
		t.Error("maps with different values incorrectly identified as same")
	}
}

func TestMSTime(t *testing.T) {
	sample := "/Date(1432187580000-0000)/"
	expect := 1432187580000 * int64(time.Millisecond)
	gt, err := MSTime(sample)
	if err != nil {
		t.Fatal(err)
	}
	if gt.UTC().UnixNano() != expect {
		t.Errorf("unexpected time conversion: %s\v", gt.String())
	}
}
