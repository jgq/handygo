package handygo

import (
	"fmt"
	"os"
	"testing"
)

func Test_AlphaNumericRCodeGenerator(t *testing.T) {

	num := 32
	results, err := GenerateRCodes(num, 8, 3, AlphaNumericRCodeGenerator, AcceptAnyRCode)

	if err != nil {
		t.Error(err)
	}
	if len(results.Results) != num {
		t.Fatal("unexpected number of rcodes generated", len(results.Results))
	}
	if results.Conflicts > 0 {
		t.Fatal("conflicts detected")
	}

	for _, token := range results.Results {
		fmt.Fprintln(os.Stderr, token)
	}
}

func Test_NumericRCodeGenerator(t *testing.T) {

	num := 32
	results, err := GenerateRCodes(num, 8, 3, NumericRCodeGenerator, AcceptAnyRCode)

	if err != nil {
		t.Error(err)
	}
	if len(results.Results) != num {
		t.Fatal("unexpected number of rcodes generated", len(results.Results))
	}
	if results.Conflicts > 0 {
		t.Fatal("conflicts detected")
	}

	for _, token := range results.Results {
		fmt.Fprintln(os.Stderr, token)
	}
}
