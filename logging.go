package handygo

import (
	"context"
	"log/slog"
)

const (
	LogAttrError = "error"
)

type contextAttributeLogger string

const CtxAttrLogger contextAttributeLogger = "slog"

func ContextWithLogger(ctx context.Context, slr *slog.Logger) context.Context {
	return context.WithValue(ctx, CtxAttrLogger, slr)
}

func LoggerFromContext(ctx context.Context) *slog.Logger {
	val := ctx.Value(CtxAttrLogger)
	if val == nil {
		return nil
	}
	if slr, ok := val.(*slog.Logger); ok {
		return slr
	}
	return nil
}

func ErrLogAttr(err error) slog.Attr {
	return slog.Any(LogAttrError, err)
}
