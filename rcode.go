package handygo

import (
	"fmt"
	"math"
	"math/rand"
	"os"
	"sync"
	"time"
)

type IsRCodeAvailableFn func(rcode string) bool
type RCodeGeneratorFn func(length int, maxTries int, rcodeAvaialble IsRCodeAvailableFn) (token string, err error)

const RCodeCharacterPool = "abcdefghijkmnopqrswxyz234569"

func AcceptAnyRCode(rcode string) bool {
	return true
}

func CharacterPoolRCodeGenerator(pool string) RCodeGeneratorFn {
	return func(length int, maxTries int, rcodeAvaialble IsRCodeAvailableFn) (token string, err error) {
		lastInPool := len(pool)
		for attempt := 0; err == nil && token == "" && attempt < maxTries; attempt++ {
			var pad []byte
			for i := 0; err == nil && i < length; i++ {
				pad = append(pad, byte(pool[rand.Intn(lastInPool)]))
			}

			candidate := string(pad)
			if rcodeAvaialble(candidate) {
				token = candidate
			}
		}
		return
	}
}

var AlphaNumericRCodeGenerator RCodeGeneratorFn

func NumericRCodeGenerator(length int, maxTries int, rcodeAvaialble IsRCodeAvailableFn) (token string, err error) {
	min := int(math.Pow10(length - 1))
	max := int(math.Pow10(length) - 1)
	rmax := max - min
	for attempt := 0; err == nil && token == "" && attempt < maxTries; attempt++ {
		num := rand.Intn(rmax) + min
		candidate := fmt.Sprintf("%d", num)
		if rcodeAvaialble(candidate) {
			token = candidate
		}
	}
	return
}

type GeneratedRCodes struct {
	Results   []string
	Conflicts int
}

func GenerateRCodes(quantity int, length int, maxTriesEach int, generator RCodeGeneratorFn, tokenAvailable IsRCodeAvailableFn) (results GeneratedRCodes, err error) {
	results = GeneratedRCodes{}
	ch := make(chan string, 4)
	tokens := make(map[string]bool)

	reading := sync.WaitGroup{}
	reading.Add(1)
	go func() {
		for token := range ch {
			fmt.Fprintln(os.Stderr, token)
			_, found := tokens[token]
			if found {
				results.Conflicts += 1
			} else {
				tokens[token] = true
			}
		}
		reading.Done()
	}()

	writing := sync.WaitGroup{}
	writing.Add(quantity)
	for i := 0; i < quantity; i++ {
		go func() {
			if err == nil {
				var token string
				token, err = generator(length, maxTriesEach, tokenAvailable)
				if err == nil {
					ch <- token
				}
			}
			writing.Done()
		}()
	}

	writing.Wait()
	close(ch)
	reading.Wait()

	for token := range tokens {
		results.Results = append(results.Results, token)
	}

	return
}

func init() {
	now := time.Now()
	rand.Seed(now.UnixNano() / int64((now.Second()/2)+1))
	AlphaNumericRCodeGenerator = CharacterPoolRCodeGenerator(RCodeCharacterPool)
}
