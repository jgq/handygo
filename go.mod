module jgquinn.com/handygo

go 1.21

require (
	github.com/gofrs/uuid v4.2.0+incompatible
	github.com/oklog/ulid/v2 v2.1.0
	github.com/spf13/cobra v1.7.0
	github.com/spf13/viper v1.0.2
	go.uber.org/zap v1.26.0
	go.uber.org/zap/exp v0.2.0
)

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/fsnotify/fsnotify v1.4.7 // indirect
	github.com/hashicorp/hcl v0.0.0-20180404174102-ef8a98b0bbce // indirect
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/magiconair/properties v1.7.6 // indirect
	github.com/mitchellh/mapstructure v0.0.0-20180220230111-00c29f56e238 // indirect
	github.com/pelletier/go-toml v1.1.0 // indirect
	github.com/spf13/afero v1.1.0 // indirect
	github.com/spf13/cast v1.2.0 // indirect
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	go.uber.org/multierr v1.10.0 // indirect
	golang.org/x/sys v0.13.0 // indirect
	golang.org/x/text v0.13.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)

retract v0.1.0 // Included occasional divide-by-zero error while seeding random.
