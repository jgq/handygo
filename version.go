package handygo

import "net/http"

// VersionString extends the core string type, adding a handler method.
type VersionString string

// VersionHandler writes version string contents back to an HTTP request.
func (v VersionString) VersionHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "text/plain")
	w.Write([]byte(v))
}
